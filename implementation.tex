\section{Implementation}
\label{sec:implementation}

We have implemented the proposed language constructs in our second-order system\footnote{Anonymized for double blind review}.
It solves second-order specifications using a \emph{ground-and-solve} approach, grounding to \emph{Quantified Boolean Formulas} (QBF).

This ground-and-solve approach is extended to support the proposed language constructs using a rewriting procedure, executed before grounding.
Some auxiliary information, however, is retained to enable optimizations during the grounding phase.

\subsection{Parametrized Theories and their Applications}
First, we detail how parametrized theories and their applications are processed.
The rewriting starts by performing a simple renaming such that every type and symbol has a \emph{unique} name.
Subsequently, while passing over the formula, whenever a parametrized theory \theory{T} application is encountered, e.g. \lstinline[mathescape, escapechar=$]|$\deref\theory{T}$(S)|, we replace the application  by the second-order formula $\phi_{\theory{T}}$.

While doing this, we replace in $\phi_{\theory{T}}$ any occurrence of the symbols declared in the vocabulary $V_\theory{T}$ by the symbols to which \theory{T} was applied.
The same substitution is performed for types in the vocabulary $V_\theory{T}$ used by arguments of \theory{T}.

It is clear that these rewritings conserve the intended semantics of theory applications: the knowledge encoded in $\phi_\theory{T}$ is simply re-expressed in terms of the symbols passed to \theory{T} at its application.

\subsection{Quantifying over Variant Worlds}
The processing of quantifications over variant worlds is less straightforward.
When encountering a quantification over variant worlds $\Box \theory{T}(S:s) : \phi$, we introduce a (set of) new variable(s) for the argument(s) S, binding them by second-order quantifications ($\forall$ for $\Box$, $\exists$ for $\Diamond$-quantifications). 
Next, we substitute the argument symbol(s) S in $\phi$ by these new variables, while substituting any application of the \outerOperator operator on these symbols by the original symbol proper.
This substitution takes care of the shadowing of symbols occurring in $\Box$ or $\Diamond$ constructs.
We call this new formula $\phi_{\mathit{sub}}$

The rewriting process, up to this point, correctly captures the semantics of $\phi$ being evaluated under new interpretations for the symbol(s) S.
Now, it must also take into account that not just any interpretation will do: only those corresponding to variant worlds that 1) satisfy the theory \theory{T} and 2) share the \commoncore{} $s$, must be considered.
The first condition can introduced using a new parametrized theory application on the newly introduced symbols, i.e. \theory{T}(S'), to be processed later on, as specified in the previous section,
The second condition can be achieved by introducing a formula $\psi_{\mathit{link}}$, which encodes the information in the \commoncore set $s$ as formulas that we call `linking statements'; for a \commoncore{} of the form $\{f(a,b)|\psi\}$, $\psi_{\mathit{link}}$ would correspond to $\forall a :: T_a : \forall b :: T_b : \psi \implies f(a,b) = f'(a,b)$. Note that $f'$ is the new symbol introduced for $f$ by the earlier translation steps, and that the type info $T_a,T_b$ can be derived from the type of $f$.
The full transformations for $\Box$ and $\Diamond$ can be summarized as follows:
\begin{align*}
\Box\theory{T}(S:s) : \phi &\rightsquigarrow \forall S' :: T : (\theory{T}(S') \land \psi_{\mathit{link}}) \Rightarrow \phi_{\mathit{sub}}.\\
\Diamond\theory{T}(S:s) : \phi &\rightsquigarrow \exists S' :: T : \theory{T}(S') \land \psi_{\mathit{link}} \land \phi_{\mathit{sub}}.	
\end{align*}

\label{sec:optimalization}
We want to highlight the fact that, where possible, by lifted evaluation, our system pre-evaluates the \commoncore set and prevents the unnecessary introduction of propositions when grounding the resulting SO formula by sharing between $S$ and $S'$.